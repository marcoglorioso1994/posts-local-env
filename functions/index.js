/* eslint-disable array-callback-return */
/* eslint-disable no-unexpected-multiline */
/* eslint-disable no-await-in-loop */
const functions = require('firebase-functions');
const swaggerUI = require("swagger-ui-express")
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

const swagger = require("./swagger.json");
const { reduceObj } = require('./utils/helper');
const { getFirestorePosts, getRandomFirestorePosts } = require('./utils/helperDB')
const { post, changes, comment, validateRequest, validateRequestLastDoc } = require('./utils/validators');
const { firestore, FieldValue, Timestamp } = require('./utils/helperFirebase')

const app = express();
app.use(bodyParser.json())
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

const MAX_POSTS = 20
const MIN_POSTS = 50

//swagger documentation endpoints
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swagger));

app.get('/:uuid', async (req, res) => {

    try {

        const uuid = req.params.uuid;

        //Check if the posts entry exists
        const doc = await firestore.collection('posts').doc(uuid).get();
        if (!doc.exists) {
            //Return or res
            res.status(404).send({ error: `Not found a posts entry with ${uuid}` });
            return
        }
        //Ref to the list col
        var ref = firestore.collection('posts').doc(uuid).collection("list");
        //Arr of user posts order by creation date desc
        var posts = (await ref.orderBy('createdAt', 'desc').get()).docs.map(doc => doc.data())

        res.json({ code: 200, uuid: uuid, posts: posts });
        return

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

app.get('/:uuid/audience', async (req, res) => {

    try {

        const uuid = req.params.uuid;

        //Check if the posts entry exists
        const doc = await firestore.collection('posts').doc(uuid).get();
        if (!doc.exists) {
            //Return or res
            res.status(404).send({ error: `Not found a posts entry with ${uuid}` });
            return
        }
        //Ref to the list col
        var ref = firestore.collection('posts').doc(uuid).collection("list");

        var audience = {};
        var likesCnt = 0
        var totalComments = 0
        //Download all posts of the user
        var posts = (await ref.get()).docs.map(doc => doc.data())

        //Sum of commentsCnt and likesCnt
        if (posts.length > 0) {
            for (let post of posts) {
                totalComments += post.commentsCnt
                likesCnt += post.likesCnt
            }
        }

        audience['totalComments'] = totalComments
        audience['likesCnt'] = likesCnt

        res.json({ code: 200, uuid: uuid, audience: audience });
        return
    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

app.post('/:uuid', validateRequest(post), async (req, res) => {

    try {

        var body = req.body;
        const uuid = req.params.uuid;

        //Check if the posts entry exists
        const doc = await firestore.collection('posts').doc(uuid).get();
        if (!doc.exists) {
            //Return or res
            res.status(404).send({ error: `Not found a posts entry with ${uuid}` });
            return
        }

        //Increase the count outfits in uuid doc in the users table
        const increment = FieldValue.increment(1);
        //Ref to the uuid doc of the users col
        let refUser = firestore.collection('users').doc(uuid)
        //Update outfits count
        await refUser.update({ 'profile.outfits': increment });

        //Get the profile user property 
        let profile = (await refUser.get()).data().profile

        //Creation on a new doc with an Cloud Firestore auto-generate ID
        var refPost = firestore.collection('posts').doc(uuid).collection("list").doc()

        //Add info about id, date of creation and author
        const postModel = {
            likes: {},
            likesCnt: 0,
            comments: [],
            commentsCnt: 0,
            id: refPost.id,
            createdBy: uuid,
            author: profile, //Set author property with the profile property
            createdAt: Timestamp.now().toMillis()
        };
        const post = Object.assign(postModel, body);
        //Write of new post
        await refPost.set(post);

        res.json({ code: 200, inserted: 1, post: post });
        return

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

app.post('/:uuid/:id/comments', validateRequest(comment), async (req, res) => {

    try {

        var body = req.body;
        const id = req.params.id;
        const uuid = req.params.uuid;

        var ref = firestore.collection('posts').doc(uuid).collection("list")
        //Check if the posts entry exists
        var doc = await ref.doc(id).get();
        if (!doc.exists) {
            res.status(404).send({ error: `Not found a post with ${id} or posts entry for ${uuid}.` });
            return
        } else {
            var comments = doc.data().comments

            comments.push({
                "user": body.user,
                "text": body.text,
                "createdAt": Timestamp.now().toMillis()
            })

            await ref.doc(id).update({
                'comments': comments
            })

            res.json({ code: 200, updated: 1, id: id, user: uuid });
            return
        }

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

app.put('/:uuid/:id/likes/:user', async (req, res) => {

    try {

        const id = req.params.id;
        const uuid = req.params.uuid;
        const user = req.params.user;

        var ref = firestore.collection('posts').doc(uuid).collection("list")
        //Check if the posts entry exists
        var doc = await ref.doc(id).get();
        if (!doc.exists) {
            res.status(404).send({ error: `Not found a post with ${id} or posts entry for ${uuid}.` });
            return
        } else {

            var refUsers = firestore.collection('users').doc(user)
            //Check if the posts entry exists
            var docUser = await refUsers.get()
            if (!docUser.exists) {
                res.status(404).send({ error: `Not found a user with ${user}` });
                return
            }

            //Update one post by id
            var likes = doc.data().likes
            var likesKey = Object.keys(likes)

            if (likesKey.length > 0) {
                if (likesKey.includes(user)) {
                    likes[user] = !likes[user]
                } else {
                    likes[user] = true
                }
            } else {
                likes[user] = true
            }

            await ref.doc(id).update({
                'likes': likes
            })

            res.json({ code: 200, updated: 1, id: id, user: uuid })
            return
        }

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

app.put('/homepost/:uuid', validateRequestLastDoc(), async (req, res) => {

    try {
        //last doc could be a empty object
        var lastDoc = req.body;
        const uuid = req.params.uuid;

        //Check if the posts entry exists
        const doc = await firestore.collection('posts').doc(uuid).get();
        if (!doc.exists) {
            //Return or res
            res.status(404).send({ error: `Not found a posts entry with ${uuid}` });
            return
        }

        //limit query param validation
        var limit = req.query.limit
        if (limit === undefined || limit === "") {
            limit = MIN_POSTS
        } else {
            limit = parseInt(limit)
            if (limit > MAX_POSTS)
                limit = MAX_POSTS
        }
        //Return a list of posts from firestore
        var [UUIDs, posts] = await getFirestorePosts(uuid, lastDoc, limit, firestore)

        var nextLastDoc

        //Check if there are posts using the community service and the lastDoc received in the request
        if (posts.length < 0) {
            //Check if there are posts using the community service and with the lastDoc obj = {} (null)
            [UUIDs, posts] = await getFirestorePosts(uuid, {}, limit, firestore)
        }

        if (posts.length < 0) {
            //Random posts without the usage of the community service
            posts = await getRandomFirestorePosts(limit, firestore)
            nextLastDoc = {}
        } else {
            //Complete the first two scenarions using the community service
            nextLastDoc = reduceObj(posts[posts.length - 1])
            nextLastDoc['UUIDs'] = UUIDs
        }

        res.json({ code: 200, uuid: uuid, lastDoc: nextLastDoc, posts: posts });
        return

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
})

app.put('/:uuid/:id', validateRequest(changes), async (req, res) => {

    try {

        var body = req.body;
        const id = req.params.id;
        const uuid = req.params.uuid;

        var ref = firestore.collection('posts').doc(uuid).collection("list")
        //Check if the posts entry exists
        var doc = await ref.doc(id).get();
        if (!doc.exists) {
            res.status(404).send({ error: `Not found a post with ${id} or posts entry for ${uuid}.` });
            return
        } else {
            //Update one post by id
            await ref.doc(id).update(body);
            res.json({ code: 200, updated: 1, id: id, user: uuid });
            return
        }

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

app.delete('/:uuid/:id', async (req, res) => {

    try {

        const id = req.params.id;
        const uuid = req.params.uuid;

        var refPost = firestore.collection('posts').doc(uuid).collection("list")
        //Check if the post exists
        var doc = await refPost.doc(id).get();
        if (!doc.exists) {
            res.status(404).send({ error: `Not found a post with ${id} or posts entry for ${uuid}.` });
            return
        } else {
            //Delete one post by id
            await refPost.doc(id).delete();

            //Decrease the count outfits in uuid doc in the users col
            const decrement = FieldValue.increment(-1);
            //Document reference
            var refUser = firestore.collection('users').doc(uuid);
            //Update outfits count
            await refUser.update({ 'profile.outfits': decrement });

            res.json({ code: 200, removed: 1, id: id, user: uuid });
            return
        }

    } catch (error) {
        console.error(error.stack);
        res.status(500).send({ error: error.message });
    }
});

// Expose Express API as a single Cloud Function:
exports.posts = functions
    .region('europe-west1')
    .https.onRequest(app);