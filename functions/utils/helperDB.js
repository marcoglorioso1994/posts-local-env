/* eslint-disable promise/always-return */
/* eslint-disable promise/catch-or-return */
const { getlistUUIDS } = require("./helperHTTP")

//Firestore query to get profiles that are matching the list uuid
async function getFirestorePosts(uuid, lastDoc, limit, firestore) {

    //Attributes arr json payload
    var uuids = []
    var posts = []
    var keys = Object.keys(lastDoc)

    if (keys.length > 0) {
        //Continue the previous list results
        uuids = lastDoc.UUIDs
        posts = await getPostsByUUIDWithPayload(uuids, lastDoc, limit, firestore)
    } else {
        uuids = (await getlistUUIDS(uuid)).data.results
        posts = await getPostsByUUIDWithEmptyPayload(uuids, limit, firestore)
    }
    return [uuids, posts]
}

/**
 * The method return an array of posts using a loop and an array of promise.
 * It is the case with an empty lastDoc payload
 * 
 * @param {*} uuids list of uuids from community servive
 * @param {*} limit limit of results
 * @param {*} firestore db instance
 * @returns 
 */
async function getPostsByUUIDWithEmptyPayload(uuids, limit, firestore) {

    var arr = []
    var ids = [...uuids]

    // don't run if there aren't any ids or a path for the collection
    if (!ids || !ids.length || !firestore) return posts;

    let batches = [];

    while (ids.length) {
        // firestore limits batches to 10
        const subIds = ids.splice(0, 10);

        // add the batch request to to a queue
        batches.push(
            new Promise(response => {
                firestore
                    .collectionGroup('list')
                    .where('createdBy', 'in', [...subIds])
                    .orderBy('createdAt', 'desc')
                    .limit(limit)
                    .get()
                    .then(results => response(results.docs.map(doc => doc.data())))
            })
        )
    }

    // after all of the data is fetched, return it
    await Promise.all(batches).then(content => {
        arr = [].concat(...content);
    })

    return arr
}

/**
 * The method return an array of posts using a loop and an array of promise.
 * It is the case with lastDoc payload
 * 
 * @param {*} uuids list of uuids from community servive
 * @param {*} limit limit of results
 * @param {*} firestore db instance
 * @returns 
 */
async function getPostsByUUIDWithPayload(uuids, lastDoc, limit, firestore) {

    var arr = []
    var ids = [...uuids]

    // don't run if there aren't any ids or a path for the collection
    if (!ids || !ids.length || !firestore) return posts;

    let batches = [];

    while (ids.length) {
        // firestore limits batches to 10
        const subIds = ids.splice(0, 10);

        // add the batch request to to a queue
        batches.push(
            new Promise(response => {
                firestore
                    .collectionGroup('list')
                    .where('createdBy', 'in', [...subIds])
                    .orderBy('createdAt', 'desc')
                    .startAfter(lastDoc.createdAt)
                    .limit(limit)
                    .get()
                    .then(results => response(results.docs.map(doc => doc.data())))
            })
        )
    }

    // after all of the data is fetched, return it
    await Promise.all(batches).then(content => {
        arr = [].concat.apply([], content)
    })

    return arr
}

/**
 * Return an array of posts using an index firestore on id field
 * In the query is used a random id with >= and < operator to get random posts in evry execution
 * The method doesn't call the community service.
 * 
 * @param {*} limit 
 * @param {*} admin 
 * @param {*} firestore 
 * @returns an array of posts to populate the homepost
 */
async function getRandomFirestorePosts(limit, firestore) {

    var random = firestore.collection('posts').doc().id

    var query = firestore
        .collectionGroup('list')
        .where('id', '>=', random)
        .limit(limit)

    let posts = (await query.get()).docs.map(doc => doc.data())

    if (posts.length === 0) {
        query = firestore
            .collectionGroup('list')
            .where('id', '<', random)
            .limit(limit)
        posts = (await query.get()).docs.map(doc => doc.data())
    }

    return posts
}

module.exports = {
    getFirestorePosts,
    getRandomFirestorePosts
}