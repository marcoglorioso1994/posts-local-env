const admin = require('firebase-admin');

var serviceAccount = require('../admin.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://dollapp-nodejs.firebaseio.com",
    storageBucket: "dollapp-nodejs.appspot.com",
    authDomain: "dollapp-nodejs.firebaseapp.com",
});

const firestore = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
const Timestamp = admin.firestore.Timestamp

async function getRemoteConfigParams() {
    var config = admin.remoteConfig();
    var template = await config.getTemplate()
    return template.parameters
}

async function getParam(key) {
    let parameters = await getRemoteConfigParams()
    return parameters[key].defaultValue.value
}

module.exports = {
    getParam,
    firestore,
    FieldValue,
    Timestamp
}