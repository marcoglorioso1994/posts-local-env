const axios = require("axios");
const { getParam } = require("./helperFirebase");

var basePathCommunity
var apiKey

async function getHttpConfig() {
    if (basePathCommunity || apiKey)
        return

    basePathCommunity = await getParam('communityGatewayUrl')
    apiKey = await getParam('key')
}


// eslint-disable-next-line consistent-return
async function getlistUUIDS(user) {

    await getHttpConfig()

    const url = `${basePathCommunity}/nodes/${user}/homepost?key=${apiKey}`;

    try {
        const res = await axios.get(url);
        if (res.status === 200)
            return res;
        else
            return null;
    } catch (err) {
        console.error(err);
    }
}

module.exports = {
    getlistUUIDS
}
