function reduceObj(obj) {
    return {
        id: obj.id,
        createdAt: obj.createdAt,
        createdBy: obj.createdBy
    }
}

module.exports = {
    reduceObj
}