
const Joi = require("joi");

const validateRequest = (schema) => async (req, res, next) => {
    const { error } = schema.validate(req.body);

    if (error) {
        return res.status(400).send(error.details[0].message);
    }
    return next();
};

const validateRequestLastDoc = () => async (req, res, next) => {
    var keys = Object.keys(req.body)

    if (keys.length > 0) {
        const { error } = lastDoc.validate(req.body)

        if (error) {
            return res.status(400).send(error.details[0].message)
        }
    }
    return next()
}

const post = Joi.object().keys({
    description: Joi.string().max(2000).strict().required(),
    style: Joi.string().valid("casual", "streetwear", "sportswear", "grunge", "cocktail", "hipster", "formal", "smart_casual", "beachwear").strict().required(),
    imageUrl: Joi.string().regex(/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/).strict().required(),
})

const changes = Joi.object().keys({
    description: Joi.string().max(2000).strict().required(),
    style: Joi.string().valid("casual", "streetwear", "sportswear", "grunge", "cocktail", "hipster", "formal", "smart_casual", "beachwear").strict().required(),
})

const lastDoc = Joi.object().keys({
    id: Joi.string().strict().required(),
    createdBy: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required(),
    createdAt: Joi.date().timestamp().required(),
    UUIDs: Joi.array().required()
})

const comment = Joi.object().keys({
    user: Joi.string().min(20).max(40).regex(/^U\w{20,40}/).trim().strict().required(),
    text: Joi.string().strict().required(),
})

module.exports = {
    post,
    changes,
    lastDoc,
    comment,
    validateRequest,
    validateRequestLastDoc
}
